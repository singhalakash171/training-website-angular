import { Component, OnInit } from '@angular/core';
import { UpcommingProgramService } from 'src/app/services/upcomming-program.service';

@Component({
  selector: 'app-programs-list',
  templateUrl: './programs-list.component.html',
  styleUrls: ['./programs-list.component.css']
})
export class ProgramsListComponent implements OnInit {

  programs: any;
  currentProgram= null;
  currentIndex = -1;
  programName ='';

  constructor(private upcommingProgramService: UpcommingProgramService) { }

  ngOnInit(): void {
    this.retrievePrograms();
  }

  retrievePrograms(): void{
    this.upcommingProgramService.getAll()
      .subscribe(
        data => {
          this.programs = data;
          console.log(data);
        },
        error => {
          console.log(error);
        }
      );
  }

  refreshList(): void {
    this.retrievePrograms();
    this.currentProgram = null;
    this.currentIndex = -1;
  }

  setActiveProgram(program, index): void {
    this.currentProgram = program;
    this.currentIndex = index;
  }

  removeAllPrograms(): void {
    this.upcommingProgramService.deleteAll()
      .subscribe(
        response => {
          console.log(response);
          this.retrievePrograms();
        },
        error => {
          console.log(error);
        });
  }

  searchTitle(): void {
    this.upcommingProgramService.findByProgramName(this.programName)
      .subscribe(
        data => {
          this.programs = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

}
