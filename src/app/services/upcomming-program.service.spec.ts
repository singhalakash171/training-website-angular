import { TestBed } from '@angular/core/testing';

import { UpcommingProgramService } from './upcomming-program.service';

describe('UpcommingProgramService', () => {
  let service: UpcommingProgramService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UpcommingProgramService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
