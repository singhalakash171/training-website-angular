import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

const baseUrl = 'http://localhost:8080/api/question/';

@Injectable({
  providedIn: 'root'
})
export class FaqService {

  constructor(private http: HttpClient) { }

  getQuestions(): Observable<any> {
    return this.http.get(baseUrl);
  }

  createQuestions(data): Observable<any> {
    return this.http.post(baseUrl, data);
  }

}
