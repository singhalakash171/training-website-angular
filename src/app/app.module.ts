import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeTopComponent } from './home-top/home-top.component';
import { HomeSlidesComponent } from './home-slides/home-slides.component';
import { AddProgramComponent } from './admin/add-program/add-program.component';
import { ProgramsDetailsComponent } from './admin/programs-details/programs-details.component';
import { ProgramsListComponent } from './admin/programs-list/programs-list.component';
import { AdminHomeProgramComponent } from './admin/admin-home-program/admin-home-program.component';
import { HomeFooterComponent } from './home-footer/home-footer.component';
import { HomeCoursetypeComponent } from './home-coursetype/home-coursetype.component';
import { DevelopmentComponent } from './course/development/development.component';
import { FaqSearchComponent } from './faq/faq-search/faq-search.component';
import { FaqAddUpdateComponent } from './faq/faq-add-update/faq-add-update.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

@NgModule({
  declarations: [
    AppComponent,
    HomeTopComponent,
    HomeSlidesComponent,
    AddProgramComponent,
    ProgramsDetailsComponent,
    ProgramsListComponent,
    AdminHomeProgramComponent,
    HomeFooterComponent,
    HomeCoursetypeComponent,
    DevelopmentComponent,
    FaqSearchComponent,
    FaqAddUpdateComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    Ng2SearchPipeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
