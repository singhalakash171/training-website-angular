import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FaqAddUpdateComponent } from './faq-add-update.component';

describe('FaqAddUpdateComponent', () => {
  let component: FaqAddUpdateComponent;
  let fixture: ComponentFixture<FaqAddUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FaqAddUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FaqAddUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
