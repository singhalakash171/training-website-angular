import { TrainingService } from './../../services/training.service';
import { Component, OnInit } from '@angular/core';
import { FaqService } from 'src/app/services/faq.service';

@Component({
  selector: 'app-faq-search',
  templateUrl: './faq-search.component.html',
  styleUrls: ['./faq-search.component.css']
})
export class FaqSearchComponent implements OnInit {
  questions: any;
  modules:any;
  currentquestion= null;
  currentIndex = -1;
  currentmodule=null;
  currentmIndex = 0;
  question ='';
  filteredArray=[];
  newarray=[];
  arr:any;
  items:any;
  searchText='';
  show=false;
  easy:any='EASY';
  medium:any='MEDIUM';
  difficult:any='DIFFICULT';

  constructor(private faqService: FaqService, private trainingService:TrainingService) {

  }

  ngOnInit(): void {
    this.retrieveModules();

    this.retrieveQuestions();
console.log(this.currentmIndex);


  }

  retrieveQuestions(): void{
    this.faqService.getQuestions()
      .subscribe(
        data => {
          this.questions = data;
          console.log(data);
        },
        error => {
          console.log(error);
        }
      );
  }

  retrieveModules(): void{
    this.trainingService.getModulesList()
      .subscribe(
        data => {
          this.modules = data;
          console.log(data);
        },
        error => {
          console.log(error);
        }
      );
  }



  refreshList(): void {
    this.retrieveQuestions();
    this.retrieveModules();
    this.currentmodule = null;
    this.currentquestion = null;
    this.currentIndex = -1;
    this.currentmIndex = 0;
    this.modules.id=1;
  }

  setActiveQuestion(question, index): void {
    this.currentquestion = question;
    this.currentIndex = index;
    this.show=!this.show;
  }
  setActiveModule(module, index): void {
    this.currentmodule = module;
    this.currentmIndex = index;

  }

  /*removeAllPrograms(): void {
    this.faqService.deleteAll()
      .subscribe(
        response => {
          console.log(response);
          this.retrieveQuestions();
        },
        error => {
          console.log(error);
        });
  }*/

  searchTitle(): void {
    this.faqService.getQuestions()
      .subscribe(
        data => {
          this.questions = data;
          console.log(data);
          this.items = this.questions.forEach(element => {

           if(this.question==element.question.trim()) {
             return this.items;
           }

          });

          console.log(this.newarray);
        },
        error => {
          console.log(error);
        });
  }



}
